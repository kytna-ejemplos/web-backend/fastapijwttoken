import os
import jwt
from fastapi import Request, Response, HTTPException
from models.user import users
from config.db import engine

async def verify_token(req: Request, res: Response):
    token = req.cookies.get('x-access-token')
    
    if token == None:
        res.status_code = 400
        raise HTTPException(400, 'No Se Encontro La Cookie \'x-access-token\'')

    try:
        decoded = jwt.decode(token, os.getenv('JWT_SECRET'), algorithms="HS256")
    except:
        raise HTTPException(500, 'Error En La Verificación Del Token De Autentificación')

    async with engine.begin() as conn:
        result = await conn.execute(users.select().where(users.c.id == decoded['id']))

    res.status_code = 200
    return result.first()