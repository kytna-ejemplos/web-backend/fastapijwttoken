from pydantic import BaseModel

class UserDto(BaseModel):
    name: str | None
    email: str | None
    password: str | None