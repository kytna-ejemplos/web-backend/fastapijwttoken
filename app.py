# Imports

from cryptography.fernet import Fernet
from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from routes.auth import auth as authRoutes
from routes.protected import protected as protectedRoutes
from dotenv import load_dotenv
from config.db import setup_database_tables
import asyncio 

# Load Environment Variables

load_dotenv()

# Create App

app = FastAPI()

# Routing

@app.get('/', response_class=HTMLResponse)
def root():
    with open('./index.html') as file:
        data = file.read()
    
    return data


app.include_router(authRoutes)
app.include_router(protectedRoutes)
    
# Setup Database

loop = asyncio.get_event_loop()
loop.create_task(setup_database_tables())