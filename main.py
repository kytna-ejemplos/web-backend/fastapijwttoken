import os
import uvicorn
import click
from cryptography.fernet import Fernet
import string
import random

def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

@click.group()
def entry_point():
    pass

@click.command()
@click.option('--port', default=8000)
def iniciar(port):
    uvicorn.run('app:app', host='127.0.0.1', port=port)

@click.command()
@click.option('--secret', default=get_random_string(20))
@click.option('--exp', default=5, help='Tiempo De Expiración En Minutos')
@click.option('--force', default=False, help='Forzar Configuración Si Ya Existe')
def configurar(secret: str, exp: int, force: bool):
    if not os.path.exists('.env') | force:
        f = open('.env', 'w')
        f.write('FERNET_KEY={key} \n'.format(key=Fernet.generate_key().decode()))
        f.write('JWT_SECRET={secret}\n'.format(secret=secret if secret is not None else get_random_string(16)))
        f.write('EXP={expirtaionTime}'.format(exp if exp is not None else 5))
        f.close()

    if not os.path.exists('database.db') | force:
        f = open('database.db', 'w')
        f.close()

entry_point.add_command(iniciar)
entry_point.add_command(configurar)

if __name__ == '__main__':
    entry_point()