import os
import jwt
from fastapi import APIRouter, Request, Depends
#from config.db import conn
#from models.user import users
from dependencies.verify_token import verify_token

protected = APIRouter()

@protected.get('/protected', dependencies=[Depends(verify_token)])
def protected_route(req: Request):
    return 'You\'re Logged'