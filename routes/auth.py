import os
from datetime import datetime, timedelta, timezone
from uuid import uuid4
from fastapi import APIRouter, Response, Form, HTTPException
from cryptography.fernet import Fernet
from models.user import users
from config.db import engine
from sqlalchemy.exc import IntegrityError
import jwt

auth = APIRouter()

#f = Fernet()

@auth.post('/api/register')
async def register(name: str = Form(), email: str = Form(), password: str = Form()):
    f = Fernet(os.getenv('FERNET_KEY'))

    if name == None:
        raise HTTPException(400, 'El Nombre Tiene Que Estar Definido')
    elif email == None:
        raise HTTPException(400, 'EL Email Tien Que Estar Definido')
    elif password == None:
        raise HTTPException(400, 'La Contraseña Tiene Que Estar Definida')

    new_user = {}
    new_user['id'] = str(uuid4())
    new_user['name'] = name
    new_user['email'] = email
    new_user['password'] = f.encrypt(password.encode('utf-8'))

    async with engine.begin() as conn:
        try:
            await conn.execute(users.insert(), new_user)
            await conn.commit()
            await conn.close()
        except IntegrityError:
            raise HTTPException(500, 'Database Integrity Error')

        return new_user
    

@auth.post('/api/login')
async def login(response: Response, email: str = Form(), password: str = Form()):
    f = Fernet(os.getenv('FERNET_KEY'))

    async with engine.begin() as conn:
        result = await conn.execute(users.select().where(users.c.email == email))
        user = result.first()
        
        if user == None:
            raise HTTPException(404, 'User Not Found')

        await conn.close()

    decryptedPassword = f.decrypt(user.password).decode()

    if (password == decryptedPassword):
        encoded_jwt = jwt.encode(
            {"id": user['id'], 
            "exp": datetime.now(tz=timezone.utc) + timedelta(minutes=int(os.getenv('EXP')))}, 
            os.getenv('JWT_SECRET'), 
            algorithm="HS256"
        )

        response.set_cookie(key='x-access-token', value=encoded_jwt)

        return encoded_jwt
    else:
        raise HTTPException(400, 'Passwords Not Matching')