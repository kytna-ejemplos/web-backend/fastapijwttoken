from sqlalchemy import Column, String, Table
from config.db import Base

users = Table(
    "users",
    Base.metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String(24), unique=True, nullable=False),
    Column("email", String, unique=True, nullable=False),
    Column("password", String(24), nullable=False)
)