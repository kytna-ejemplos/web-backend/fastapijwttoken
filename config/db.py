from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker, declarative_base


engine = create_async_engine('sqlite+aiosqlite:///database.db?check_same_thread=False')

Base = declarative_base(engine)

async def setup_database_tables():
    from models.user import users

    conn = await engine.begin()
    await conn.run_sync(Base.metadata.create_all)
    await conn.close()

    await engine.dispose()
