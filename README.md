# Fast API JWT Token

Ejemplo de autentificación con JSON Web Token utilizando FastAPI en python

# Uso

La apliación e configura e inicia a partir el archivo `main.py`

## Configurar La Aplicación

Antes de iniciar el proyecto se necesita configurar.

Para configurarlo automatica mente utiliza el siguiente comando

```
python3 main.py configurar
```

### `--secret`
 
Con la opción `--secret` puedes especificar manualmente el secreto de JWT, si no utilizas la opción se genera uno aleatorio

#### Ejemplo

```
python3 main.py configurar --secret='Example Secret'
```

### `--exp`

Con la opción `--exp` puedes elegir el tiempo que tarda en expirar en minutos los tokens generados, por defecto tardan 5 minutos en expirar.


#### Ejemplo
```
python3 main.py configurar --exp=10
```

### `--force`

Con la opción `--force` sobreescribes cualquier configuración existente, por defecto esta desactivado.

#### Ejemplo
```
python3 main.py configurar --force
```

## Iniciar El Proyecto

Para iniciar el proyecto en le puerto 8000 **(habiendo configurado antes)** utiliza el siguiente commando.

```
python3  main.py iniciar
```

### `--port`
Para especificar un puerto concreto utiliza la opción `--port`, por defecto la aplicación se ejecuta en el puerto 8000.

#### Ejemplo
```
python3 main.py iniciar --port 1234
```